package daos;

import dbConnection.ConnectionFactory;
import models.Client;
import models.Product;

import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDao implements Dao<Product> {

    @Override
    public List<Product> getAll(DefaultTableModel tableModel) throws SQLException {
        List<Product> products = new ArrayList<>();
        String SQLQuerry = "SELECT * FROM Product";
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQLQuerry);
        ResultSet resultSet = statement.executeQuery();
        int i = 0;
        while(resultSet.next()){
            products.add(new Product(resultSet.getInt("id"),resultSet.getInt("pret"),resultSet.getInt("cantitate"),resultSet.getString("nume")));
            int id = products.get(i).getId();
            int pret = products.get(i).getPret();
            int cantitate = products.get(i).getCantitate();
            String nume = products.get(i).getNume();
            tableModel.addRow(new Object[]{id,pret,cantitate,nume});
            i++;
        }

        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);

        return products;
    }

    @Override
    public void addNew(Product product) {
        String SQLQuerry = "INSERT INTO Product(id,pret,cantitate,nume) VALUES(" + product.getId() +","+product.getPret()+","+product.getCantitate()+",\""+product.getNume()+"\")";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);

    }

    @Override
    public void edit(Product product) {
        String SQLQuerry = "UPDATE Product SET id=" +product.getId() +" ,pret=" + product.getPret() +", cantitate=" +product.getCantitate() +", nume=\"" +product.getNume()+ "\" WHERE id="+product.getId();

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }

    @Override
    public void delete(Product product) {
        String SQLQuerry = "DELETE FROM Product WHERE id="+product.getId();
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }
}
