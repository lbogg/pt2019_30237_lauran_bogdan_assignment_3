package daos;

import dbConnection.ConnectionFactory;
import models.Order;
import sun.net.ResourceManager;

import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDao implements Dao<Order> {

    @Override
    public List<Order> getAll(DefaultTableModel tableModel) throws SQLException {
        List<Order> orders = new ArrayList<>();
        String SQLQuerry = "SELECT * FROM orders";
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQLQuerry);
        ResultSet resultSet = statement.executeQuery();
        int i = 0;
        while(resultSet.next()){
            orders.add(new Order(resultSet.getInt("id"),resultSet.getInt("idClient"),resultSet.getInt("idProdus"),resultSet.getInt("cantitate")));
            int id = orders.get(i).getId();
            int idClient = orders.get(i).getIdClient();
            int idProdus = orders.get(i).getIdProdus();
            int cantitate = orders.get(i).getCantitate();
            tableModel.addRow(new Object[]{id,idClient,idProdus,cantitate});
            i++;
        }

        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);

        return orders;
    }

    @Override
    public void addNew(Order order) {
        String SQLQuerry = "INSERT INTO orders VALUES(" + order.getId()+','+ order.getIdClient()+','+ order.getIdProdus()+','+ order.getCantitate()+')';

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }

    @Override
    public void edit(Order order) {
    }

    @Override
    public void delete(Order order) {

    }
}
