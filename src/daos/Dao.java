package daos;

import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    List<T> getAll(DefaultTableModel tableModel ) throws SQLException;

    void addNew(T t);

    void edit(T t);

    void delete(T t);
}
