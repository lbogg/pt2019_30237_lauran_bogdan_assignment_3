package daos;

import dbConnection.ConnectionFactory;
import models.Client;
import models.Order;

import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;

public class ClientDao implements Dao<Client> {
    @Override
    public List<Client> getAll(DefaultTableModel tableModel) throws SQLException {
        List<Client> clients = new ArrayList<>();
        String SQLQuerry = "SELECT * FROM Client";
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQLQuerry);
        ResultSet resultSet = statement.executeQuery();
        int i = 0;
        while(resultSet.next()){

            clients.add(new Client(resultSet.getInt("id"),resultSet.getString("name"),resultSet.getString("address"),resultSet.getString("email"),resultSet.getInt("age")));
            int id = clients.get(i).getId();
            String nume = clients.get(i).getName();
            String address = clients.get(i).getAddress();
            String email = clients.get(i).getEmail();
            int age = clients.get(i).getAge();
            tableModel.addRow(new Object[]{id,nume,address,email,age});
            i++;
        }

        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);

        return clients;
    }

    @Override
    public void addNew(Client client) {
        String SQLQuerry = "INSERT INTO Client(id,name,address,email,age) VALUES(" + client.getId() + ",\"" + client.getName() + "\",\"" + client.getAddress() + "\",\"" + client.getEmail() + "\"," + client.getAge() + ")";

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);

    }

    @Override
    public void edit(Client client) {
        String SQLQuerry = "UPDATE Client SET id=" +client.getId() +" ,name=\"" + client.getName() +"\", address=\"" +client.getAddress() +"\", email=\"" +client.getEmail() +"\", age="+client.getAge() +" WHERE id="+client.getId();

        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }

    @Override
    public void delete(Client client) {
        String SQLQuerry = "DELETE FROM Client WHERE id="+client.getId();
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(SQLQuerry);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }
}
