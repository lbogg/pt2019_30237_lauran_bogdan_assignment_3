package bll;

import java.lang.reflect.Field;
import java.util.Vector;

public class Reflection {
    public static String[] retrieveProperties(Object object) {
        Field[] fieldVector = object.getClass().getDeclaredFields();
        String[] strings = new String[fieldVector.length];
        for (int i = 0; i < fieldVector.length; i++) {
            fieldVector[i].setAccessible(true);
            try {
                strings[i] = fieldVector[i].getName();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return strings;
    }
}
