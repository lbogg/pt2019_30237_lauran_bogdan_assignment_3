package bll;

import daos.Dao;
import daos.OrderDao;
import daos.ProductDao;
import models.Order;
import models.Product;

import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderBll {

    private OrderDao orderDao = new OrderDao();
    private ProductBll productBll = new ProductBll();
    private ProductDao productDao = new ProductDao();

    public void insertOrder(Order order,DefaultTableModel tableModel){
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Product p: products){
            if(p.getId() == order.getIdProdus()){
                p.setCantitate(p.getCantitate()-order.getCantitate());
                try {
                    productBll.updateProduct(p,tableModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        orderDao.addNew(order);
    }

    public void showOrders(DefaultTableModel tableModel) throws Exception {
        List<Order> orders = new ArrayList<>();
        try {
            orders = orderDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(orders.size() == 0){
            throw new Exception("Nu exista momentan comenzi plasate");
        }
    }
}
