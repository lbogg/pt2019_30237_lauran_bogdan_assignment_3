package bll;

import daos.ClientDao;
import models.Client;

import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientBll {

    private ClientDao clientDao = new ClientDao();

    public Integer[] getIds(DefaultTableModel tableModel){
        List<Client> clients = new ArrayList<>();

        try {
            clients = clientDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Integer[] ids = new Integer[clients.size()];
        for(int i = 0; i < clients.size();i++){
            ids[i] = clients.get(i).getId();
        }
        return ids;
    }

    public void insertClient(Client client, DefaultTableModel tableModel) throws Exception {
        List<Client> clients = new ArrayList<>();
        try {
            clients = clientDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Client c:clients){
            if(c.getId() == client.getId()){
                throw new Exception("Clientul exista deja in baza de date");
            }
        }
        clientDao.addNew(client);
    }

    public void deleteClient(int id, DefaultTableModel tableModel) throws Exception {
        List<Client> clients = new ArrayList<>();
        try {
            clients = clientDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Client c:clients){
            if(c.getId() == id){
                clientDao.delete(c);
            }
        }
        throw new Exception("Clientul cu acest id nu exista in baza de date");
    }

    public void updateClient(Client client, DefaultTableModel tableModel) throws Exception {
        List<Client> clients = new ArrayList<>();
        try {
            clients = clientDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Client c:clients){
            if(c.getId() == client.getId()){
                clientDao.edit(client);
            }
        }
        throw new Exception("Clientul cu acest id nu exista in baza de date");
    }

    public void showClients(DefaultTableModel tableModel) throws Exception {
        List<Client> clients = new ArrayList<>();
        try {
            clients = clientDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(clients.size() == 0){
            throw new Exception("Nu exista momentan clienti");
        }
    }

}
