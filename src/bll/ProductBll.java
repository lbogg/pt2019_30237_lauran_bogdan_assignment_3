package bll;

import daos.ClientDao;
import daos.Dao;
import daos.ProductDao;
import models.Product;

import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductBll {

    private ProductDao productDao = new ProductDao();

    public int getPret(int id, DefaultTableModel tableModel){
        int pret = 0;
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(Product p: products){
            if(p.getId() == id){
                pret = p.getPret();
            }
        }
        return pret;
    }

    public Integer[] getIds(DefaultTableModel tableModel){
        List<Product> products = new ArrayList<>();

        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Integer[] ids = new Integer[products.size()];
        for(int i = 0; i < products.size();i++){
            ids[i] = products.get(i).getId();
        }
        return ids;
    }

    public void insertProduct(Product product, DefaultTableModel tableModel) throws Exception {
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Product p:products){
            if(p.getId() == product.getId()){
                throw new Exception("Produsul exista deja in baza de date");
            }
        }
        productDao.addNew(product);
    }

    public void deleteProduct(int id, DefaultTableModel tableModel) throws Exception {
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Product p:products){
            if(p.getId() == id){
                productDao.delete(p);
            }
        }
    }

    public void updateProduct(Product product, DefaultTableModel tableModel) throws Exception {
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Product p:products){
            if(p.getId() == product.getId()){
                productDao.edit(product);
            }
        }
    }

    public void showProducts(DefaultTableModel tableModel) throws Exception {
        List<Product> products = new ArrayList<>();
        try {
            products = productDao.getAll(tableModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(products.size() == 0){
            throw new Exception("Nu exista momentan produse in stoc");
        }
    }
}
