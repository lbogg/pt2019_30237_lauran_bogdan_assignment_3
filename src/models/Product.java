package models;

public class Product {

    private int id;
    private int pret;
    private int cantitate;
    private String nume;

    public Product(int id, int pret, int cantitate, String nume) {
        this.id = id;
        this.pret = pret;
        this.cantitate = cantitate;
        this.nume = nume;
    }

    public Product(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}
