package validators;

import models.Client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements Validator<Client> {
    public static  final Pattern mailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public void validate(Client client) throws Exception {
        if(!mailPattern.matcher(client.getEmail()).matches()){
            throw new Exception("Invalid Email");
        }
    }
}
