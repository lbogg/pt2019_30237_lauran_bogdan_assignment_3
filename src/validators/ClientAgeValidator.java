package validators;

import models.Client;

public class ClientAgeValidator implements Validator<Client> {
    @Override
    public void validate(Client client) throws Exception {
        if(client.getAge() < 18 || client.getAge() > 100){
            throw new Exception("Age is not valid");
        }
    }
}
