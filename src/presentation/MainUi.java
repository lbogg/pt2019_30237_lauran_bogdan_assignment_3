package presentation;

import javax.swing.*;
import java.awt.*;

public class MainUi extends JFrame {

    private JPanel jPanel;
    private JButton clientBtn,adminBtn;

    public MainUi(){
        this.setSize(new Dimension(400,400));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        initElements();
    }

    private void initElements(){
        this.clientBtn = new JButton("Place an order");
        this.adminBtn = new JButton("Admin");
        this.jPanel = new JPanel();
        createListeners();
    }

    private void createListeners(){
        this.jPanel.add(clientBtn);
        this.jPanel.add(adminBtn);
        this.add(jPanel);
        this.adminBtn.addActionListener(e -> {
            UserInterface userInterface = new UserInterface();
            userInterface.setVisible(true);
        });
        this.clientBtn.addActionListener(e -> {
            OrderPlacingUI orderPlacingUI = new OrderPlacingUI();
            orderPlacingUI.setVisible(true);
        });
    }
}
