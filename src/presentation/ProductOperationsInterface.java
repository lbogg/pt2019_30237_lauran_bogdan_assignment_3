package presentation;

import bll.ClientBll;
import bll.ProductBll;
import bll.Reflection;
import models.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;

public class ProductOperationsInterface extends JFrame {

    public JPanel jPanel;
    private JButton addBtn,deleteBtn,editBtn,viewAllBtn;
    Product product = new Product();
    String[] columns = Reflection.retrieveProperties(product);
    DefaultTableModel tableModel = new DefaultTableModel(null, columns);

    public ProductOperationsInterface(){
        this.setSize(new Dimension(400,400));
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        initElements();
    }

    public void initElements(){
        this.addBtn = new JButton("Add Product");
        this.deleteBtn = new JButton("Delete Product");
        this.editBtn = new JButton("Edit Product");
        this.viewAllBtn = new JButton("Show Products");
        this.jPanel = new JPanel();

        addListeners();
    }

    public void addListeners(){
        this.jPanel.add(addBtn);
        this.jPanel.add(deleteBtn);
        this.jPanel.add(editBtn);
        this.jPanel.add(viewAllBtn);
        this.add(jPanel);

        this.viewAllBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(800,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            Product product = new Product();
            String[] columns = Reflection.retrieveProperties(product);
            DefaultTableModel tableModel = new DefaultTableModel(null, columns);
            ProductBll productBll = new ProductBll();
            try {
                productBll.showProducts(tableModel);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            JTable table = new JTable(tableModel);
            JTableHeader tableHeader = table.getTableHeader();
            tableHeader.setPreferredSize(new Dimension(50,20));
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout(4,1));
            jPanel.add(tableHeader);
            jPanel.add(table);
            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

        });

        this.editBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("Id Produs");
            JTextField nameTxt = new JTextField();
            nameTxt.setText("Nume Produs");
            JTextField pretTxt = new JTextField();
            pretTxt.setText("Pret Produs");
            JTextField cantitateTxt = new JTextField();
            cantitateTxt.setText("Cantitate Produs");
            JButton editProduct = new JButton("Update");

            GroupLayout layout = new GroupLayout(jPanel);
            jPanel.setLayout(layout);
            layout.setAutoCreateGaps(true);
            layout.setAutoCreateContainerGaps(true);
            GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

            hGroup.addGroup(layout.createParallelGroup().addComponent(idTxt).addComponent(nameTxt).addComponent(pretTxt));
            hGroup.addGroup(layout.createParallelGroup().addComponent(cantitateTxt).addComponent(editProduct));
            //hGroup.addGroup(layout.createParallelGroup().addComponent(ageTxt).addComponent(addClient));
            layout.setHorizontalGroup(hGroup);

            GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(idTxt).addComponent(cantitateTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(editProduct));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(pretTxt));
            //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addClient));
            layout.setVerticalGroup(vGroup);

            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

            editProduct.addActionListener(e1 -> {
                ProductBll productBll = new ProductBll();
                Product product = new Product(Integer.parseInt(idTxt.getText()),Integer.parseInt(pretTxt.getText()),Integer.parseInt(cantitateTxt.getText()),nameTxt.getText());
                try {
                    productBll.updateProduct(product,tableModel);
                } catch (Exception e2) {
                    e2.getMessage();
                }
            });
        });

        this.deleteBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("idProdus");
            JButton deleteProduct = new JButton("Delete");

            jPanel.add(idTxt);
            jPanel.add(deleteProduct);

            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

            deleteProduct.addActionListener(e1 -> {
                ProductBll productBll = new ProductBll();
                try {
                    productBll.deleteProduct(Integer.parseInt(idTxt.getText()),tableModel);
                } catch (Exception e2) {
                    e2.getMessage();
                }
            });
        });

        this.addBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400, 400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel4 = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("Id Produs");
            JTextField nameTxt = new JTextField();
            nameTxt.setText("Nume Produs");
            JTextField pretTxt = new JTextField();
            pretTxt.setText("Pret Produs");
            JTextField cantitateTxt = new JTextField();
            cantitateTxt.setText("Cantitate Produs");
            JButton addProduct = new JButton("Add");

            GroupLayout layout = new GroupLayout(jPanel4);
            jPanel4.setLayout(layout);
            layout.setAutoCreateGaps(true);
            layout.setAutoCreateContainerGaps(true);
            GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

            hGroup.addGroup(layout.createParallelGroup().addComponent(idTxt).addComponent(nameTxt).addComponent(pretTxt));
            hGroup.addGroup(layout.createParallelGroup().addComponent(cantitateTxt).addComponent(addProduct));
            //hGroup.addGroup(layout.createParallelGroup().addComponent(ageTxt).addComponent(addClient));
            layout.setHorizontalGroup(hGroup);

            GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(idTxt).addComponent(cantitateTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addProduct));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(pretTxt));
            //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addClient));
            layout.setVerticalGroup(vGroup);

            cViewFrame.setContentPane(jPanel4);
            cViewFrame.setVisible(true);

            addProduct.addActionListener(e12 -> {
                ProductBll productBll = new ProductBll();
                Product product = new Product(Integer.parseInt(idTxt.getText()),Integer.parseInt(pretTxt.getText()),Integer.parseInt(cantitateTxt.getText()),nameTxt.getText());

                try {
                    productBll.insertProduct(product,tableModel);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });
        });
    }
}
