package presentation;

import bll.ClientBll;
import bll.Reflection;
import models.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientOperationsInterface extends JFrame {

    public JPanel jPanel;
    private JButton addBtn,deleteBtn,editBtn,viewAllBtn;
    Client client = new Client();
    String[] columns = Reflection.retrieveProperties(client);
    DefaultTableModel tableModel = new DefaultTableModel(null, columns);

    public ClientOperationsInterface(){
        this.setSize(new Dimension(400,400));
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        initElements();
    }

    public void initElements(){
        this.addBtn = new JButton("Add Client");
        this.deleteBtn = new JButton("Delete Client");
        this.editBtn = new JButton("Edit Client");
        this.viewAllBtn = new JButton("Show Clients");
        this.jPanel = new JPanel();

        addListeners();
    }

    public void addListeners(){
        this.jPanel.add(addBtn);
        this.jPanel.add(deleteBtn);
        this.jPanel.add(editBtn);
        this.jPanel.add(viewAllBtn);
        this.add(jPanel);

        this.viewAllBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(800,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            Client client = new Client();
            String[] columns = Reflection.retrieveProperties(client);
            DefaultTableModel tableModel = new DefaultTableModel(null, columns);
            ClientBll clientBll = new ClientBll();
            try {
                clientBll.showClients(tableModel);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            JTable table = new JTable(tableModel);
            JTableHeader tableHeader = table.getTableHeader();
            tableHeader.setPreferredSize(new Dimension(50,20));
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout(4,1));
            jPanel.add(tableHeader);
            jPanel.add(table);
            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

        });

        this.editBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("Id Client");
            JTextField nameTxt = new JTextField();
            nameTxt.setText("Nume Client");
            JTextField addrTxt = new JTextField();
            addrTxt.setText("Adresa Client");
            JTextField emailTxt = new JTextField();
            emailTxt.setText("Email Client");
            JTextField ageTxt = new JTextField();
            ageTxt.setText("Varsta Client");
            JButton editClient = new JButton("Update");

            GroupLayout layout = new GroupLayout(jPanel);
            jPanel.setLayout(layout);
            layout.setAutoCreateGaps(true);
            layout.setAutoCreateContainerGaps(true);
            GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

            hGroup.addGroup(layout.createParallelGroup().addComponent(idTxt).addComponent(nameTxt).addComponent(addrTxt));
            hGroup.addGroup(layout.createParallelGroup().addComponent(emailTxt).addComponent(ageTxt).addComponent(editClient));
            //hGroup.addGroup(layout.createParallelGroup().addComponent(ageTxt).addComponent(addClient));
            layout.setHorizontalGroup(hGroup);

            GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(idTxt).addComponent(emailTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(ageTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(addrTxt).addComponent(editClient));
            //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addClient));
            layout.setVerticalGroup(vGroup);

            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

            editClient.addActionListener(e1 -> {
                ClientBll clientBll = new ClientBll();
                Client client = new Client(Integer.parseInt(idTxt.getText()),nameTxt.getText(),addrTxt.getText(),emailTxt.getText(),Integer.parseInt(ageTxt.getText()));
                try {
                    clientBll.updateClient(client,tableModel);
                } catch (Exception e2) {
                    e2.getMessage();
                }
            });
        });

        this.deleteBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("idClient");
            JButton deleteClient = new JButton("Delete");

            jPanel.add(idTxt);
            jPanel.add(deleteClient);

            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);

            deleteClient.addActionListener(e1 -> {
                ClientBll clientBll = new ClientBll();
                //Client client = new Client();
                try {
                    clientBll.deleteClient(Integer.parseInt(idTxt.getText()),tableModel);
                } catch (Exception e2) {
                    e2.getMessage();
                }
            });
        });

        this.addBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(400, 400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            JPanel jPanel1 = new JPanel();
            JPanel jPanel2 = new JPanel();
            JPanel jPanel3 = new JPanel();
            JPanel jPanel4 = new JPanel();
            JPanel jPanel5 = new JPanel();
            JPanel jPanel6 = new JPanel();
            JTextField idTxt = new JTextField();
            idTxt.setText("Id Client");
            JTextField nameTxt = new JTextField();
            nameTxt.setText("Nume Client");
            JTextField addrTxt = new JTextField();
            addrTxt.setText("Adresa Client");
            JTextField emailTxt = new JTextField();
            emailTxt.setText("Email Client");
            JTextField ageTxt = new JTextField();
            ageTxt.setText("Varsta Client");
            JButton addClient = new JButton("Add");

            GroupLayout layout = new GroupLayout(jPanel4);
            jPanel4.setLayout(layout);
            layout.setAutoCreateGaps(true);
            layout.setAutoCreateContainerGaps(true);
            GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

            hGroup.addGroup(layout.createParallelGroup().addComponent(idTxt).addComponent(nameTxt).addComponent(addrTxt));
            hGroup.addGroup(layout.createParallelGroup().addComponent(emailTxt).addComponent(ageTxt).addComponent(addClient));
            //hGroup.addGroup(layout.createParallelGroup().addComponent(ageTxt).addComponent(addClient));
            layout.setHorizontalGroup(hGroup);

            GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(idTxt).addComponent(emailTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(ageTxt));
            vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(addrTxt).addComponent(addClient));
            //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addClient));
            layout.setVerticalGroup(vGroup);

            cViewFrame.setContentPane(jPanel4);
            cViewFrame.setVisible(true);

            addClient.addActionListener(e12 -> {
                ClientBll clientBll = new ClientBll();
                Client client = new Client(Integer.parseInt(idTxt.getText()),nameTxt.getText(),addrTxt.getText(),emailTxt.getText(),Integer.parseInt(ageTxt.getText()));

                try {
                    clientBll.insertClient(client,tableModel);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            });
        });
    }
}
