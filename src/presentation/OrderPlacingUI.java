package presentation;

import bll.ClientBll;
import bll.OrderBll;
import bll.ProductBll;
import bll.Reflection;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import daos.ClientDao;
import models.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

public class OrderPlacingUI extends JFrame {

    private JPanel jPanel;
    private JComboBox<Integer> jComboBox;
    private JComboBox<Integer> jComboBox1;
    private JTextField cantitateTxt;
    private JTextField idTxt;
    private JButton placeOrder;
    private JButton showOrders;
    private JLabel jLabel1,jLabel2;
    private ClientBll clientBll = new ClientBll();
    private ProductBll productBll = new ProductBll();
    private Integer[] clientIds = new Integer[100];
    private Integer[] productIds = new Integer[100];
    private Order order = new Order();
    private String[] columns = Reflection.retrieveProperties(order);
    private DefaultTableModel tableModel = new DefaultTableModel(null, columns);


    public OrderPlacingUI(){
        this.setSize(new Dimension(400,400));
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        initElements();
    }

    public void initElements(){
        clientIds = clientBll.getIds(tableModel);
        productIds = productBll.getIds(tableModel);
        this.jComboBox = new JComboBox<>(clientIds);
        this.jComboBox1 = new JComboBox<>(productIds);
        this.cantitateTxt = new JTextField("Intoduceti Cantitate");
        this.idTxt = new JTextField("Id Comanda");
        this.placeOrder = new JButton("Place Order");
        this.jPanel = new JPanel();
        this.jLabel1 = new JLabel("ID Client:");
        this.jLabel2 = new JLabel("ID Produs:");
        this.showOrders = new JButton("View Orders");
        addListeners();
    }

    public void addListeners(){
        GroupLayout layout = new GroupLayout(jPanel);
        jPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

        hGroup.addGroup(layout.createParallelGroup().addComponent(jLabel1).addComponent(jComboBox).addComponent(jLabel2).addComponent(jComboBox1).addComponent(cantitateTxt).addComponent(idTxt).addComponent(placeOrder).addComponent(showOrders));
       /* hGroup.addGroup(layout.createParallelGroup().addComponent(jComboBox1));
        hGroup.addGroup(layout.createParallelGroup().addComponent(cantitateTxt));
        hGroup.addGroup(layout.createParallelGroup().addComponent(idTxt));
        hGroup.addGroup(layout.createParallelGroup().addComponent(placeOrder));*/
        layout.setHorizontalGroup(hGroup);

        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jLabel1));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jComboBox));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jLabel2));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(jComboBox1));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(cantitateTxt));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(idTxt));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(placeOrder));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(showOrders));
        //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt).addComponent(addClient));
        layout.setVerticalGroup(vGroup);

        this.add(jPanel);

        showOrders.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(1000,800));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            Order order = new Order();
            String[] columns = Reflection.retrieveProperties(order);
            DefaultTableModel tableModel = new DefaultTableModel(null, columns);
            OrderBll orderBll = new OrderBll();
            try {
                orderBll.showOrders(tableModel);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            JTable table = new JTable(tableModel);
            JTableHeader tableHeader = table.getTableHeader();
            tableHeader.setPreferredSize(new Dimension(30,10));
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout( 4,1));
            jPanel.add(tableHeader);
            jPanel.add(table);
            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);
        });

        placeOrder.addActionListener(e -> {
            OrderBll orderBll = new OrderBll();
            Order order = new Order(Integer.parseInt(idTxt.getText()),Integer.parseInt(jComboBox.getSelectedItem().toString()),Integer.parseInt(jComboBox1.getSelectedItem().toString()),Integer.parseInt(cantitateTxt.getText()));

            try {
                getReceipt("Bill.pdf");
                orderBll.insertOrder(order,tableModel);

            } catch (Exception e1) {
                e1.printStackTrace();
            }

        });
    }
    private void getReceipt(String filename) throws FileNotFoundException, DocumentException {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        PdfWriter.getInstance(document,new FileOutputStream(filename));
        document.open();
        Paragraph title = new Paragraph("\t\t Bill \n\n",FontFactory.getFont(FontFactory.HELVETICA,
                18, Font.BOLD));
        document.add(title);
        document.add(new Paragraph("Data : "+ LocalDate.now()));
        document.add(new Paragraph("Ora : "+ LocalTime.now()));
        document.add(new Paragraph("Id Comanda : "+idTxt.getText()));
        document.add(new Paragraph("Id Client : "+jComboBox.getSelectedItem()));
        document.add(new Paragraph("Id Produs : "+jComboBox1.getSelectedItem()));
        document.add(new Paragraph("Cantitate : "+cantitateTxt.getText()));
        document.add(new Paragraph("Pret :"+Integer.parseInt(cantitateTxt.getText())*productBll.getPret(Integer.parseInt(jComboBox1.getSelectedItem().toString()),tableModel)+" RON"));

        document.close();
    }
}
