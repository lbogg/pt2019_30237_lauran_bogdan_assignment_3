package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserInterface extends JFrame {

    private JPanel mainPanel;
    private JButton clientBtn,productBtn;

    public UserInterface(){
        this.setSize(new Dimension(400,400));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        initElements();
    }

    private void initElements(){
        this.clientBtn = new JButton("Client Operations");
        this.productBtn = new JButton("Product Operations");
        this.mainPanel = new JPanel();
        createListeners();
    }

    private void createListeners(){
        this.mainPanel.add(clientBtn);
        this.mainPanel.add(productBtn);
        this.add(mainPanel);
        this.clientBtn.addActionListener(e -> {
            ClientOperationsInterface clientOperationsInterface = new ClientOperationsInterface();
            clientOperationsInterface.setVisible(true);
        });
        this.productBtn.addActionListener(e -> {
            ProductOperationsInterface productOperationsInterface = new ProductOperationsInterface();
            productOperationsInterface.setVisible(true);
        });

    }
}
